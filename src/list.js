import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import { useNavigation } from '@react-navigation/native'; 
import { useState, useEffect } from "react";
import axios from 'axios'; 

const Lista = () => {
  const [users, setUsers] = useState([]);
  const navigation = useNavigation(); 

  useEffect(() => {
    obterDados();
  }, []);

  async function obterDados() {
    try {
      const response = await axios.get('https://jsonplaceholder.typicode.com/users'); 
      setUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  const onPressItem = (details) => {
    navigation.navigate("Detalhes", { details }); 
  };

  return (
    <View>
      <FlatList
        data={users} 
        keyExtractor={(item) => item.id.toString()} 
        renderItem={({ item }) => (
          <TouchableOpacity 
            onPress={() => onPressItem(item)}>
               
            <Text>Acessar usuário:  {item.name}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default Lista;


